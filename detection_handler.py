import cv2

# Load haarcascade data once from script directory
haar_classifier = cv2.CascadeClassifier(
    'haarcascade_frontalface_default.xml')

def detect_faces(image, _scaleFactor, _minNeighbors):

    # Run classifier to detect faces within image
    face_boundary, rejectLevels, levelWeights = haar_classifier.detectMultiScale3(
        image,
        scaleFactor=_scaleFactor,
        minNeighbors=_minNeighbors,
        minSize=(50, 50),
        outputRejectLevels=True
    )

    # Check if a face exists
    if len(face_boundary) == 0:
        return None

    # Assumption made that only one face should exist in image, take the detected face with highest probability.
    return face_boundary[levelWeights.argmax()]

def run_detection(image, scaleFactor, minNeighbors):
    if haar_classifier is None:
        return None, 'Failed_To_Load_Classifier'

    if image.size == 0:
        return None, 'Image_Empty'

    detection = detect_faces(image, scaleFactor, minNeighbors)
    if detection is None:
        return None, 'No_Detection'

    return detection, ''

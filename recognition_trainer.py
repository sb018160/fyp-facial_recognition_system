import os
import cv2
import numpy as np
import time
import random

import detection_handler


def capture_training():

   # Start Video Capture
    print('starting capture')
    time.sleep(1)

    camera_video = cv2.VideoCapture(0 + cv2.CAP_DSHOW)

    if not camera_video.isOpened():
        return None, -1, 'Cannot_Open_Webcam'

    # Set capture window properties
    cv2.namedWindow('Capture', cv2.WINDOW_NORMAL)
    cv2.setWindowProperty(
        'Capture', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    cv2.setWindowProperty(
        'Capture', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)

    # Attempt to capture 10 images of face for training out of 5 seconds
    captured_frames = []

    end_time = int(time.time()) + 5

    while len(captured_frames) < 10 and end_time > int(time.time()):

        # Get capture frame
        ret, frame = camera_video.read()

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        detected_boundary, error = detection_handler.run_detection(
            frame, 1.4, 11)

        # If a face is detected
        if detected_boundary is not None:
            # Get's x,y,w,h values from the detection rect and crops image
            x = detected_boundary[0]
            y = detected_boundary[1]
            w = detected_boundary[2]
            h = detected_boundary[3]
            cropped_face = frame[y:y + h, x:x + w]

            captured_frames.append(cropped_face)

            # Draw green box on the latest frame around the detected face
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

        cv2.imshow('Capture', frame)

       # Check for Keyboard input inbetween frames.
       # Also waits slightly before next capture to attempt to get better 
       # variance between captures due to natural movement
        key = cv2.waitKey(100)

        # Break out of the program if ESC is pressed (OpenCV KeyCode 27 is ESC key)
        if key == 27:
            camera_video.release()
            cv2.destroyAllWindows()
            print('Escape pressed, Exiting')
            return None, -1, 'Exit_Pressed'

    camera_video.release()
    cv2.destroyAllWindows()

    # Check enough images captured
    if len(captured_frames) != 10:
        return None, -1, 'Not_Enough_Samples'
    return captured_frames, 1, ''


def train_recognition(training_path):
    # Create recognizer object
    lbp_recognizer = cv2.face.LBPHFaceRecognizer_create()

    # Create directory to save trained recognizer if doesn't already exist
    if not os.path.exists('./lbp_recognizer'):
        os.makedirs('./lbp_recognizer')

    print('Loading training images')
    labels, images = load_training_data(training_path)

    print('Beginning face capture')
    time.sleep(1)
    # Capture images of user's face
    capture, code, error = capture_training()
    
    if code == -1: # If error encountered during capture, exit
        return -1, error
    
    # Get last 8 images out of total 10
    images.extend(capture[-8:])
    new_label = np.full((8), max(labels) + 1) # Set user label to new value
    labels.extend(new_label)
    
    # Shuffle label order to obfuscate using a random seed
    seed = random.randint(1, 1000000)

    random.shuffle(images, random.seed(seed))
    random.shuffle(labels, random.seed(seed))

    labels = np.array(labels)

    # Pick random label to swap user data with for further obfuscation
    swap_label = random.randint(2, max(labels)-1)

    labels[labels == swap_label] = -1
    labels[labels == max(labels)] = swap_label
    labels[labels == -1] = max(labels) + 1
    
    # For further obfuscation, hash the label values
    hashed_labels = []
    for label in labels:
        hashed_labels.append(hash(str(label)) % 100000000)

    print('Training system')
    lbp_recognizer.train(images, np.array(hashed_labels))
    print('Saving trained system')
    lbp_recognizer.save('lbp_recognizer/training_data.yml')
    print('System saved')
    return 1, ''

def load_training_data(training_path):
    # Get a list of all image names from the given training path
    image_names = os.listdir(training_path)
    images = []
    labels = []

    for name in image_names:
        image = cv2.imread(os.path.join(training_path, name))
        # Convert image to greyscale
        grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Append image from training data
        images.append(grey_image)
        # Append label, assuming that format has the label at the start 
        # of the image name, split by an underscore
        labels.append(int(name.split('_')[0]))

    return labels, images

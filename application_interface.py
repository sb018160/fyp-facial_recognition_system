import os
import tkinter as tk
from tkinter import filedialog

import recognition_trainer
import recognition_handler
import cryptography_handler


def run_gui():

    # Set as global so that the root can be manipulated within other functions
    global gui_root

    gui_root = tk.Tk()
    gui_root.title("Facial encryption FYP")
    gui_root.geometry("300x85")

    gui_root.lift()
    gui_root.attributes("-topmost", True)

    text_frame = tk.Frame(gui_root)
    text_frame.pack(side='top')
    
    # Add information text box to window
    info_label = tk.Label(text_frame, anchor='nw', justify='left', padx=5, pady=3,
                          text="Please choose options from below.\nIf this is your first time using, please run training")
    info_label.pack()

    bottom_frame = tk.Frame(gui_root)
    bottom_frame.pack(side='bottom')

    # Add buttons to window
    tk.Button(bottom_frame, text='Train', command=lambda: train_system()).pack(
        side='left', padx=8, pady=10)
    tk.Button(bottom_frame, text='Encrypt',
              command=lambda: encrypt_file()).pack(side='left', padx=8)
    tk.Button(bottom_frame, text='Decrypt',
              command=lambda: decrypt_file()).pack(side='left', padx=8)
    tk.Button(bottom_frame, text='Exit', fg='red',
              command=gui_root.destroy).pack(side='left', padx=8)

    gui_root.mainloop()


def train_system():
    print('training system')
    # Assume that the training path is always found in the script folder and called "training_data"
    code, error = recognition_trainer.train_recognition('./training_data/')
    if code != 1:
        print('Error encountered: ' + str(error))
        return

    print('training complete')
    return


def get_file(process):
    
    # Create new window
    root = tk.Tk()
    root.lift()
    root.withdraw()

    if process == 'Decryption':
        # Limit file selection to .encrypt filetypes if decrypting 
        file_path = filedialog.askopenfilename(title='Select File', filetypes=(
            ("Encrypted Files", "*.encrypt"), ("All Files", "*.*")))
    elif process == 'Encryption':
        file_path = filedialog.askopenfilename(title='Select File')
    else:
        print('Unknown process chosen, exiting')
        return None
    
    # Close window after selection
    root.destroy()
    
    return file_path


def encrypt_file():
    # Destroy main menu window
    gui_root.destroy()

    # Check trained recognizer exists
    if not os.path.exists('./lbp_recognizer/training_data.yml'):
        print('trained recognizer not found, run training first')
        return

    # Let user select file
    file_path = get_file('Encryption')

    if file_path == '' or None:
        print('No file chosen, quitting')
        return

    # Let user input password
    password_text, entered = get_password_input()
    if not entered:
        print('Password entry closed, exiting')
        return
    
    # Get salt from LBP recognizer
    print('starting recognition')
    salt, error = recognition_handler.main()
    if salt == -1: # Check for error
        print('Error encountered: ' + error)
        return

    # Encrypt file using gathered data
    print('encrypting file')
    cryptography_handler.run_cryptography(file_path, password_text, salt, 'Encryption')


def decrypt_file():
    # Destroy main menu window
    gui_root.destroy()

    # Check trained recognizer exists
    if not os.path.exists('./lbp_recognizer/training_data.yml'):
        print('trained recognizer not found, run training first')
        return

    # Let user select file
    file_path = get_file('Decryption')

    if file_path == '' or None:
        print('No file chosen, quitting')
        return

    # Let user input password
    password_text, entered = get_password_input()
    
    if not entered:
        print('Password entry closed, exiting')
        return
    
    # Get salt from LBP recognizer
    print('starting recognition')
    salt, error = recognition_handler.main()
    if salt == -1: # Check for error
        print('Error encountered: ' + error)
        return

    # Decrypt file using gathered data
    print('decrypting file')
    cryptography_handler.run_cryptography(file_path, password_text, salt, 'Decryption')


def get_password_input():

    global password_entered
    global password_root
    
    password_entered = True
    password_text = None
    
    # Create tkinter window for password entry and submission
    password_root = tk.Tk()
    password_root.title('Enter Password')

    password_root.lift()
    password_root.attributes("-topmost", True)

    password_input = tk.StringVar()

    info_label = tk.Label(password_root, anchor='nw', justify='left', padx=3,
                          text="Please enter a password for encryption.\nIf left blank then just facial recognition will be used")
    info_label.pack(side='top')
    # Replace input text with * values
    tk.Entry(password_root, textvariable=password_input,
             show='*', width=20).pack(side='left')
    tk.Button(password_root, text='Submit',
              command=password_root.destroy).pack(side='left')

    password_root.protocol("WM_DELETE_WINDOW", password_closed)
    password_root.mainloop()
    
    password_text = password_input.get()
    
    return password_text, password_entered

def password_closed():
    global password_entered
    global password_root
    password_entered = False
    password_root.destroy()
    
if __name__ == "__main__":
    run_gui()

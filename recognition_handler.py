import cv2
import time
import statistics
import numpy as np

import detection_handler


def main():

    # Load trained recognizer
    print('Loading facial recognizer')
    lbp_recognizer = cv2.face.LBPHFaceRecognizer_create()
    lbp_recognizer.read('lbp_recognizer/training_data.yml')

    # Start Video Capture
    print('starting capture')
    time.sleep(1)

    camera_video = cv2.VideoCapture(0 + cv2.CAP_DSHOW)

    if not camera_video.isOpened():
        return -1, 'Cannot_Open_Webcam'

    # Set capture window properties
    cv2.namedWindow('Capture', cv2.WINDOW_NORMAL)
    cv2.setWindowProperty(
        'Capture', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    cv2.setWindowProperty(
        'Capture', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)

    # Run for 5 seconds
    end_time = int(time.time()) + 5

    detected_labels = []

    # Begin recognizer loop
    while end_time > int(time.time()):  # Loop for 5 seconds
        # Get capture frame
        ret, frame = camera_video.read()

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        detected_boundary, error = detection_handler.run_detection(
            frame, 1.4, 11)

        # If a face is detected
        if detected_boundary is not None:
            # Get's x,y,w,h values from the detection rect and crops image
            x = detected_boundary[0]
            y = detected_boundary[1]
            w = detected_boundary[2]
            h = detected_boundary[3]
            cropped_face = frame[y:y + h, x:x + w]

            # Attempt to recognize detected face
            recognized_label, recognizer_confidence = lbp_recognizer.predict(
                cropped_face)

            # Add to list of detections if the confidence is high enough
            if(recognizer_confidence > 0.9):
                detected_labels.append(recognized_label)

            # Draw green box on the latest frame around the detected face
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

        # If error raised by detector then quit out
        elif error != '' or error != 'No_Detection':
            camera_video.release()
            cv2.destroyAllWindows()
            return -1, error

        # Display new captured frame on window
        cv2.imshow('Capture', frame)

        # Check for Keyboard input inbetween frames.
        key = cv2.waitKey(1)

        # Break out of the program if ESC is pressed (OpenCV KeyCode 27 is ESC key)
        if key == 27:
            camera_video.release()
            cv2.destroyAllWindows()
            print('Escape pressed, Exiting')
            return -1, 'Exit'

    camera_video.release()
    cv2.destroyAllWindows()

    # Check if enough faces were recognised, minimum 10
    if len(detected_labels) < 10:
        # Exit with error if not enough detected
        return -1, 'Not_Enough_Labels'

    # Get most recognized label
    detected_label = statistics.mode(detected_labels)
    # Get histogram positions for this label
    labels_list = np.array(lbp_recognizer.getLabels())
    histogram_indexes = np.where(labels_list == detected_label)[0]

    # Get histogram data for this label
    histograms = [lbp_recognizer.getHistograms()[i] for i in histogram_indexes]

    # Sum of all label histograms becomes salt
    salt = sum(sum([sum(x) for x in histograms]))

    # Return salt
    return salt, ''

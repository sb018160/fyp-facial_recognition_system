import base64
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
        
def run_cryptography(path, password, salt, process):
    # Create key from salt and password
    kdf = PBKDF2HMAC(algorithm=hashes.SHA256(),length=32,salt=bytes(salt),iterations=100000)
    key = base64.urlsafe_b64encode(kdf.derive(bytes(password, 'utf-8')))
    f = Fernet(key)
    
    # Encrypt or decrypt based on process
    if process == 'Encryption':
        run_encryption(f, path)
    elif process == 'Decryption':
        run_decryption(f,path)
    else:
        print('Unknown cryptography process passed, cancelled')
    
def run_encryption(function, path):
    # Attempt to open file using the given filepath
    with open(path, "rb") as file:
        # Read file data
        file_data = file.read() 
        # Run encryption on the file data and save to memory
        encrypted_data = function.encrypt(file_data) 
    
    # Attempt to create a new file with .encrypt type for encrypted data
    with open(path + '.encrypt', "wb") as file:
        # Write encrypted data to the new file
        file.write(encrypted_data)
    
def run_decryption(function, path):
    # Attempt to open file using the given filepath
    with open(path, "rb") as file:
        # Read file data
        encrypted_data = file.read()
        # Run decryption on the encrypted data and save to memory
        try:
            decrypted_data = function.decrypt(encrypted_data)
        except:
            print('Incorrect key generated, cannot decrypt')
            return
            
    # Attempt to create a new file for decrypted data removing .encrypt type
    with open(path.replace('.encrypt',''), "wb") as file:
        # Write encrypted data to the new file
        file.write(decrypted_data)
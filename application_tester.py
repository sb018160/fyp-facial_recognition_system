import numpy as np
import random
import cv2
import time

# Import classes to test
import cryptography_handler
import detection_handler
import recognition_trainer
import recognition_handler


def run_tests():

    # Test Haar detector and variables
    # detection_accuracy_test()

    # Test the recognition accuracy
    # accuracy = []
    # average_confidence = []
    # true_confidence = []
    # failure_confidence = []
    
    # for i in range(100):
    #     print(i)
    #     a, b, c, d = recognition_accuracy_test()
    #     accuracy.append(a)
    #     average_confidence.append(b)
    #     true_confidence.append(c)
    #     failure_confidence.append(d)
    
    # print("Total Accuracy: ", sum(accuracy)/100,
    #       "Average confidence: ", sum(average_confidence)/100,
    #       "\nAverage confidence of correct recognitions: ", sum(true_confidence)/100,
    #       "Average confidence of failures: ", sum(failure_confidence)/100)

    encryption_time_total = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    decryption_time_total = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    for i in range(50):
        print(i)
        e_times, d_times = encryption_time_test()
        for i in range(len(e_times)):
            encryption_time_total[i] += e_times[i]
            decryption_time_total[i] += d_times[i]

    mean_encryption_times = [x / 50 for x in encryption_time_total]
    mean_decryption_times = [x / 50 for x in decryption_time_total]
    
    print(str(mean_encryption_times))
    print(str(mean_decryption_times))

def recognition_accuracy_test():
    results_prediction = []
    results_confidence = []
    results_correct = []
    true_labels = []

    # Load the training images and labels
    all_labels, all_training_images = recognition_trainer.load_training_data(
        './training_data/')
    unique_labels = np.unique(all_labels)
    # For each label, choose one training image to remove and use for testing

    for label in unique_labels:
        # Copy arrays containing all images
        labels = all_labels[:]
        training_images = all_training_images[:]
        # Get indexes of these labels
        label_indexes = np.where(labels == label)[0]
        # Choose a random image to remove
        image_number = random.randint(0, labels.count(label) - 1)
        # Store image used to test accuracy and remove from 
        # the list of training images
        test_image = training_images[label_indexes[image_number]]
        training_images.pop(label_indexes[image_number])
        labels.pop(label_indexes[image_number])

        # Create LBP system and train with data
        lbp_recognizer = cv2.face.LBPHFaceRecognizer_create()
        lbp_recognizer.train(training_images, np.array(labels))

        # Attempt to recognize saved image that was removed from training
        recognized_label, recognizer_confidence = lbp_recognizer.predict(
            test_image)

        # Save the results to array, results include made prediction,
        # confidence and whether the prediction was correct
        results_prediction.append(recognized_label)
        results_confidence.append(recognizer_confidence)
        results_correct.append(recognized_label == label)
        true_labels.append(label)

    # Calculate result figures and display
    average_confidence = sum(results_confidence) / len(results_confidence)
    failure_indexes = np.where(np.array(results_correct) == False)[0]
    if(len(failure_indexes) == 0):
        failure_confidence = 'N/A'
        truth_confidence = average_confidence
    else:
        failure_sum = sum([results_confidence[index]
                           for index in failure_indexes])
        failure_confidence = failure_sum / len(failure_indexes)
        truth_confidence = ((sum(results_confidence) - failure_sum) /
                            (len(results_confidence) - len(failure_indexes)))

    accuracy = results_correct.count(True) / len(results_correct)

    print("Total Accuracy: ", accuracy,
          "Average confidence: ", average_confidence,
          "\nAverage confidence of correct recognitions: ", truth_confidence,
          "Average confidence of failures: ", failure_confidence)
    
    if failure_confidence == 'N/A':
        failure_confidence = 0
        
    return accuracy, average_confidence, truth_confidence, failure_confidence

def detection_accuracy_test():
    # Load the training images and ignore labels
    _, test_images = recognition_trainer.load_training_data(
        './detector_testing/')
    detection_list = []

    # Change below values for testing the effects
    scaleFactor = 1.4
    minNeighbours = 11

    # For each image, attempt to find detection
    for i in range(len(test_images)):
        image = test_images[i]
        detection, error = detection_handler.run_detection(
            image, scaleFactor, minNeighbours)
        if detection is not None:
            detection_list.append(detection)
            x = detection[0]
            y = detection[1]
            w = detection[2]
            h = detection[3]
            cropped_detection = image[y:y + h, x:x + w]
            cv2.imwrite('./detected_images/' + str(i) +
                        '.jpg', cropped_detection)
        else:
            cv2.imwrite('./detected_images/fail_' + str(i) +
                        '.jpg', image)
    print(len(detection_list), ' detected out of ', len(test_images))
    return detection_list


def encryption_time_test():
    size_files = ["5MB.zip","10MB.zip","20MB.zip","50MB.zip","100MB.zip","200MB.zip","512MB.zip","1GB.bin"]
    directory = './testing_filesize/'
    
    encryption_times = []
    decryption_times = []
    for file in size_files:
        # Set password and salt as small values, should not affect time much
        password = "123"
        salt = 456
        path = directory + file
        time_before = time.perf_counter()
        cryptography_handler.run_cryptography(path, password, salt, 'Encryption')
        time_encrypt = time.perf_counter()
        cryptography_handler.run_cryptography(path + '.encrypt', password, salt, 'Decryption')
        time_end = time.perf_counter()
        
        encrypt_length = time_encrypt - time_before
        decrypt_length = time_end - time_encrypt
        
        encryption_times.append(encrypt_length)
        decryption_times.append(decrypt_length)
        
    for i in range(len(size_files)):
        print(size_files[i], '  encryption time: ', str(encryption_times[i]), '  decryption time: ', str(decryption_times[i]))
            
    return encryption_times, decryption_times


if __name__ == "__main__":
    run_tests()
